<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bootstrap Form</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<form method="" action="" class="form-horizontal col-md-10 col-md-offset-1">
    <div class="form-group">
        <label class="col-md-2"for="fname">Name:</label>
        <div class="col-md-10">
            <input type="text"class="form-control" id="fname" name="fname"/>

        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2"for="email">Email:</label>
        <div class="col-md-10">
            <input type="text"class="form-control" id="email" name="email"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2"for="passward">Passward:</label>
        <div class="col-md-10">
            <input type="text" class="form-control"id="passward" name="passward"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2" for="mobile">Mobile No:</label>
        <div class="col-md-10">
             <input type="text" class="form-control"id="mobile" name="mobile"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2"for="fileUpload">File Upload:</label>
        <div class="col-md-10">
            <input type="file" id="fileUpload" name="fileUpload"/>
        </div>
    </div>
    <div class="check-box col-md-offset-2">
        <label>
            <input type="checkbox" value=""/>Check Me Out
        </label>
    </div>
    <button type="submit" class="col-md-offset-2 btn btn-default">Submit</button>
</form>
</body>
</html>