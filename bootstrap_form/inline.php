<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bootstrap Form</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<form method="" action="" class="form-inline col-md-10 col-md-offset-1">
    <div class="form-group">
        <label for="fname">Name:</label>
        <input type="text"class="form-control" id="fname" name="fname"/>

    </div>
    <div class="form-group">
        <label for="email">Email:</label>
        <input type="text"class="form-control" id="email" name="email"/>
    </div>
    <div class="form-group">
        <label for="passward">Passward:</label>
        <input type="text" class="form-control"id="passward" name="passward"/>
    </div>
    <div class="form-group">
        <label for="mobile">Mobile No:</label>
        <input type="text" class="form-control"id="mobile" name="mobile"/>
    </div>
    <div class="form-group">
        <label for="fileUpload">File Upload:</label>
        <input type="file" id="fileUpload" name="fileUpload"/>
    </div>
    <div class="check-box">
        <label>
            <input type="checkbox" value=""/>Check Me Out
        </label>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>
</body>
</html>